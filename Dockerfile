FROM alpine:3.11

ENV LOG_FORMAT=logfmt
ENV LOG_LEVEL=debug
ENV TELEMETRY_ADDR=0.0.0.0:10000
ENV APP_HTTPADDR=0.0.0.0:8000
ENV APP_GRPCADDR=0.0.0.0:8001

RUN apk add --update --no-cache ca-certificates tzdata bash curl
RUN mkdir -p "/usr/local/bin/conf" && chmod -R 777 "/usr/local/bin"

SHELL ["/bin/bash", "-c"]

WORKDIR /usr/local/bin

ADD dapp /usr/local/bin/dapp
ADD config.toml /usr/local/bin/config.toml

EXPOSE 8000 8001 10000
CMD dapp --http-addr=$APP_HTTPADDR --grpc-addr=$APP_GRPCADDR --telemetry-addr=$TELEMETRY_ADDR --config=config.toml
