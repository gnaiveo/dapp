package common

import (
	"context"
)

type ErrorHandler interface {
	Handle(err error)
	HandleContext(ctx context.Context, err error)
}

type NoopErrorHandler struct{}

func (NoopErrorHandler) Handle(_ error)                           {}
func (NoopErrorHandler) HandleContext(_ context.Context, _ error) {}
