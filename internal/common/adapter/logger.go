package adapter

import (
	"context"

	"logur.dev/logur"

	"dapp/internal/common"
)

type Logger struct {
	logur.LoggerFacade

	extractor ContextExtractor
}

type ContextExtractor func(ctx context.Context) map[string]interface{}

func NewLogger(logger logur.LoggerFacade) *Logger {
	return &Logger{
		LoggerFacade: logger,
	}
}

func NewContextAwareLogger(logger logur.LoggerFacade, extractor ContextExtractor) *Logger {
	return &Logger{
		LoggerFacade: logur.WithContextExtractor(logger, logur.ContextExtractor(extractor)),
		extractor:    extractor,
	}
}

func (l *Logger) WithFields(fields map[string]interface{}) common.Logger {
	return &Logger{
		LoggerFacade: logur.WithFields(l.LoggerFacade, fields),
		extractor:    l.extractor,
	}
}

func (l *Logger) WithContext(ctx context.Context) common.Logger {
	if l.extractor == nil {
		return l
	}

	return l.WithFields(l.extractor(ctx))
}
