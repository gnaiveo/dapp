package log

type Config struct {
	Format  string
	Level   string
	NoColor bool
}
