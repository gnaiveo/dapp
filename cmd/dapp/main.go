package main

import (
	"dapp/internal/app"
	"dapp/internal/platform/database"
	"dapp/internal/platform/log"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"contrib.go.opencensus.io/integrations/ocsql"
	"emperror.dev/errors/match"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sagikazarmark/ocmux"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/trace"
	"google.golang.org/grpc"

	"github.com/cloudflare/tableflip"
	"github.com/oklog/run"
	appkitrun "github.com/sagikazarmark/appkit/run"
	"golang.org/x/net/context"
	"logur.dev/logur"

	"go.opencensus.io/zpages"

	"emperror.dev/emperror"
	"emperror.dev/errors"
	logurhandler "emperror.dev/handler/logur"
	"github.com/sagikazarmark/appkit/buildinfo"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	version    string
	commitHash string
	buildDate  string
)

const friendlyAppName = "Dapp"

func main() {
	v, p := viper.New(), pflag.NewFlagSet(friendlyAppName, pflag.ExitOnError)

	configure(v, p)

	p.String("config", "", "Configuration file")
	p.Bool("version", false, "Show version information")

	_ = p.Parse(os.Args[1:])

	if v, _ := p.GetBool("version"); v {
		fmt.Printf("%s version %s (%s) built on %s\n", friendlyAppName, version, commitHash, buildDate)

		os.Exit(0)
	}

	if c, _ := p.GetString("config"); c != "" {
		v.SetConfigFile(c)
	}

	err := v.ReadInConfig()
	_, configFileNotFound := err.(viper.ConfigFileNotFoundError)
	if !configFileNotFound {
		emperror.Panic(errors.Wrap(err, "failed to read configuration"))
	}

	var config Configuration
	err = v.Unmarshal(&config)
	emperror.Panic(errors.Wrap(err, "failed to unmarshal configuration"))

	err = config.Process()
	emperror.Panic(errors.WithMessage(err, "failed to process configuration"))

	// Create logger (first thing after configuration loading)
	logger := log.NewLogger(config.Log)

	// Override the global standard library logger to make sure everything uses our logger
	log.SetStandardLogger(logger)

	if configFileNotFound {
		logger.Warn("configuration file not found")
	}

	err = config.Validate()
	if err != nil {
		logger.Error(err.Error())

		os.Exit(3)
	}

	// configure error handler
	errorHandler := logurhandler.New(logger)
	defer emperror.HandleRecover(errorHandler)

	buildInfo := buildinfo.New(version, commitHash, buildDate)

	logger.Info("starting application", buildInfo.Fields())

	// Telemetry starting
	telemetryRouter := http.NewServeMux()
	telemetryRouter.Handle("/buildinfo", buildinfo.HTTPHandler(buildInfo))

	// Register pprof endpoints
	telemetryRouter.Handle("/debug/pprof/", http.DefaultServeMux)

	// Zpages debug
	zpages.Handle(telemetryRouter, "/debug")

	// configure graceful restart
	upg, _ := tableflip.New(tableflip.Options{})

	// Do an upgrade on SIGHUP
	go func() {
		ch := make(chan os.Signal, 1)
		signal.Notify(ch, syscall.SIGHUP)
		for range ch {
			logger.Info("graceful reloading")

			_ = upg.Upgrade()
		}
	}()

	// Register SQL stat views
	ocsql.RegisterAllViews()

	// Connect to the database
	logger.Info("connecting to database")
	dbConnector, err := database.NewConnector(config.Database)
	emperror.Panic(err)

	var db = sql.OpenDB(dbConnector)
	defer db.Close()

	// Record DB stats every 5 seconds until we exit
	defer ocsql.RecordStats(db, 5*time.Second)()

	// Group http handlers
	var group run.Group

	// Set up telemetry server
	{
		const name = "telemetry"
		logger := logur.WithField(logger, "server", name)

		logger.Info("listening on address", map[string]interface{}{"address": config.Telemetry.Addr})

		ln, err := upg.Fds.Listen("tcp", config.Telemetry.Addr)
		emperror.Panic(err)

		server := &http.Server{
			Handler:  telemetryRouter,
			ErrorLog: log.NewErrorStandardLogger(logger),
		}
		defer server.Close()

		group.Add(
			func() error { return server.Serve(ln) },
			func(err error) { _ = server.Shutdown(context.Background()) },
		)
	}

	// Set up app server
	{
		const name = "app"
		logger := logur.WithField(logger, "server", name)

		httpRouter := mux.NewRouter()
		httpRouter.Use(ocmux.Middleware())

		cors := handlers.CORS(
			handlers.AllowedOrigins([]string{"*"}),
			handlers.AllowedMethods([]string{http.MethodGet, http.MethodPost, http.MethodPatch, http.MethodDelete}),
			handlers.AllowedHeaders([]string{"content-type"}),
		)

		httpServer := &http.Server{
			Handler: &ochttp.Handler{
				// Handler: httpRouter,
				Handler: cors(httpRouter),
				StartOptions: trace.StartOptions{
					Sampler:  trace.AlwaysSample(),
					SpanKind: trace.SpanKindServer,
				},
				IsPublicEndpoint: true,
			},
			ErrorLog: log.NewErrorStandardLogger(logger),
		}
		defer httpServer.Close()

		grpcServer := grpc.NewServer(grpc.StatsHandler(&ocgrpc.ServerHandler{
			StartOptions: trace.StartOptions{
				Sampler:  trace.AlwaysSample(),
				SpanKind: trace.SpanKindServer,
			},
			IsPublicEndpoint: true,
		}))
		defer grpcServer.Stop()

		app.InitializeApp(httpRouter, grpcServer, db, errorHandler)

		logger.Info("listening on address", map[string]interface{}{"address": config.App.HttpAddr})

		httpLn, err := upg.Fds.Listen("tcp", config.App.HttpAddr)
		emperror.Panic(err)

		logger.Info("listening on address", map[string]interface{}{"address": config.App.GrpcAddr})

		grpcLn, err := upg.Fds.Listen("tcp", config.App.GrpcAddr)
		emperror.Panic(err)

		group.Add(
			func() error { return httpServer.Serve(httpLn) },
			func(err error) { _ = httpServer.Shutdown(context.Background()) },
		)
		group.Add(
			func() error { return grpcServer.Serve(grpcLn) },
			func(err error) { grpcServer.GracefulStop() },
		)
	}

	// Setup signal handler
	group.Add(run.SignalHandler(context.Background(), syscall.SIGINT, syscall.SIGTERM))

	// Setup graceful restart
	group.Add(appkitrun.GracefulRestart(context.Background(), upg))

	err = group.Run()
	emperror.WithFilter(errorHandler, match.As(&run.SignalError{}).MatchError).Handle(err)
}
