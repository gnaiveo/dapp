package main

import (
	"os"
	"strings"
	"time"

	"dapp/internal/platform/database"
	"dapp/internal/platform/log"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type Configuration struct {
	Log       log.Config
	App       appConfig
	Database  database.Config
	Telemetry struct {
		Addr string
	}
}

func (Configuration) Process() error {
	return nil
}

func (c Configuration) Validate() error {
	return nil
}

type appConfig struct {
	HttpAddr string
	GrpcAddr string
	Storage  string
}

func (c appConfig) Validate() error {
	return nil
}

func configure(v *viper.Viper, p *pflag.FlagSet) {
	// Viper settings
	v.AddConfigPath(".")

	// Environment variable settings
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	v.AllowEmptyEnv(true)
	v.AutomaticEnv()

	// Global Configuration
	v.SetDefault("shutdownTimeout", 15*time.Second)
	if _, ok := os.LookupEnv("NO_COLOR"); ok {
		v.SetDefault("no_color", true)
	}

	// Log Configuration
	v.SetDefault("log.format", "json")
	v.SetDefault("log.level", "info")
	v.RegisterAlias("log.noColor", "no_color")

	// Telemetry configuration
	p.String("telemetry-addr", ":10000", "Telemetry HTTP server address")
	_ = v.BindPFlag("telemetry.addr", p.Lookup("telemetry-addr"))
	v.SetDefault("telemetry.addr", ":10000")

	// App Configuration
	p.String("http-addr", ":8000", "App HTTP server address")
	_ = v.BindPFlag("app.httpAddr", p.Lookup("http-addr"))
	v.SetDefault("app.httpAddr", ":8000")

	p.String("grpc-addr", ":8001", "App GRPC server address")
	_ = v.BindPFlag("app.grpcAddr", p.Lookup("grpc-addr"))
	v.SetDefault("app.grpcAddr", ":8001")

	// Database Configuration
	_ = v.BindEnv("database.host")
	v.SetDefault("database.port", 3306)
	_ = v.BindEnv("database.user")
	_ = v.BindEnv("database.pass")
	_ = v.BindEnv("database.name")
	v.SetDefault("database.params", map[string]string{
		"collation": "utf8mb4_general_ci",
	})
}
